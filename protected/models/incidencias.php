<?php

/**
 * This is the model class for table "incidencias".
 *
 * The followings are the available columns in table 'incidencias':
 * @property string $id_incidencia
 * @property string $id_usuario
 * @property string $id_empleado
 * @property string $titulo
 * @property string $descripcion
 * @property string $fecha
 * @property string $hora
 * @property string $estatus
 * @property string $area
 * @property string $comentarios
 */
class incidencias extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'incidencias';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_usuario, id_empleado, titulo, descripcion, fecha, hora, estatus, area, comentarios', 'required'),
			array('id_usuario, id_empleado', 'length', 'max'=>11),
			array('titulo', 'length', 'max'=>20),
			array('descripcion', 'length', 'max'=>100),
			array('comentarios', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_incidencia, id_usuario, id_empleado, titulo, descripcion, fecha, hora, estatus, area, comentarios', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_incidencia' => 'Id Incidencia',
			'id_usuario' => 'Id Usuario',
			'id_empleado' => 'Id Empleado',
			'titulo' => 'Titulo',
			'descripcion' => 'Descripcion',
			'fecha' => 'Fecha',
			'hora' => 'Hora',
			'estatus' => 'Estatus',
			'area' => 'Area',
			'comentarios' => 'Comentarios',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_incidencia',$this->id_incidencia,true);

		$criteria->compare('id_usuario',$this->id_usuario,true);

		$criteria->compare('id_empleado',$this->id_empleado,true);

		$criteria->compare('titulo',$this->titulo,true);

		$criteria->compare('descripcion',$this->descripcion,true);

		$criteria->compare('fecha',$this->fecha,true);

		$criteria->compare('hora',$this->hora,true);

		$criteria->compare('estatus',$this->estatus,true);

		$criteria->compare('area',$this->area,true);

		$criteria->compare('comentarios',$this->comentarios,true);

		return new CActiveDataProvider('incidencias', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return incidencias the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}