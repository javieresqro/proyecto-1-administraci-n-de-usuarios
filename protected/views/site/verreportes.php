<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

	$this->pageTitle=Yii::app()->name . ' - Incidencias no resueltas';
	$this->breadcrumbs=array(
		'Incidencias no resueltas',
	);

	$id_usuario =  Yii::app()->user->id;

	$incidencia = CHttpRequest::getParam('id');
	if($incidencia == 0)
	{
		$conexion = new CDbConnection("mysql:host=localhost;dbname=usuariocruge","root","CRVCLJ");
		$conexion->active = true;

		$sql = "SELECT COUNT(*) FROM incidencias WHERE estatus='Nueva'";
	
		$instruccion = $conexion->createCommand($sql);
		$resultado = $instruccion->query();

		while(($fila = $resultado -> read()) !== false) {
			$total=$fila['COUNT(*)'];
		}

		if($total == 0)
		{
			echo "No hay incidencias registradas hasta el momento.";
		}

		else {
			$sql = 
				"SELECT incidencias.id_incidencia,incidencias.id_usuario,incidencias.fecha,incidencias.hora,incidencias.titulo,incidencias.descripcion,incidencias.estatus FROM incidencias,empleados WHERE incidencias.estatus='Nueva' AND incidencias.area = empleados.especialidad";
			//echo $sql;
			$instruccion = $conexion->createCommand($sql);
			$resultado = $instruccion->query();
			
			echo "<table>";
			echo "<tr><td># incidencia</td><td># usuario</td><td>Fecha</td><td>Hora</td><td>Título</td><td>Descripción</td><td>Situación</td></tr>";
			while(($fila = $resultado -> read()) !== false) {
				echo "<tr>
					  <td>"  . $fila['id_incidencia'] . "</td>
					  <td> " . $fila['id_usuario']. "</td>
					  <td> " . $fila['fecha']. "</td>
					  <td> " . $fila['hora']. "</td>
					  <td> " . $fila['titulo']. "</td>
					  <td> " . $fila['descripcion']. "</td>
					  <td> " . $fila['estatus']. "</td>
					  <td><button onclick=\"javascript:window.location='index.php?r=site/verreportes&id=" . $fila['id_incidencia'] . "';\">Manejar esta incidencia</button></td>
					  </tr>";
				//CVarDumper::dump($fila,10,true);
				//die();
			}
			echo "</table>";

			$conexion->active = false;
		}
	}
	else { 


		$conexion = new CDbConnection("mysql:host=localhost;dbname=usuariocruge","root","CRVCLJ");
		$conexion->active = true;

		$sql = "SELECT id_usuario,fecha,hora,titulo,descripcion FROM incidencias WHERE id_incidencia='" . $incidencia ."'";
		
		$instruccion = $conexion->createCommand($sql);
		$resultado = $instruccion->query();

		while(($fila = $resultado -> read()) !== false) {
			$id_usuario = $fila['id_usuario'];
			$fecha = $fila['fecha'];
			$hora = $fila['hora'];
			$titulo = $fila['titulo'];
			$descripcion = $fila['descripcion'];
		}

		?>

		<p>Por favor llene el siguiente formulario con los datos de su problema:</p>

		<div class="form">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'enableClientValidation'=>true,
			'clientOptions'=>array(
				'validateOnSubmit'=>true,
			),
		)); ?>

		<p class="note">Es importante llenar todos los campos.</p>

		<div class="row">
			<?php echo $form->labelEx($model,'id_usuario'); ?>
			<?php echo $form->textField($model,'id_usuario',array('readonly'=>true,'value'=>$id_usuario)); ?>
			<?php echo $form->error($model,'id_usuario'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'id_incidencia'); ?>
			<?php echo $form->textField($model,'id_incidencia',array('readonly'=>true,'value'=>$incidencia)); ?>
			<?php echo $form->error($model,'id_incidencia'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'fecha'); ?>
			<?php echo $form->dateField($model,'fecha',array('readonly'=>true,'value'=>$fecha)); ?>
			<?php echo $form->error($model,'fecha'); ?>
		</div>
		
		<div class="row">
			<?php echo $form->labelEx($model,'hora'); ?>
			<?php echo $form->timeField($model,'hora',array('readonly'=>true,'value'=>$hora)); ?>
			<?php echo $form->error($model,'hora'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'titulo'); ?>
			<?php echo $form->textField($model,'titulo',array('readonly'=>true,'value'=>$titulo)); ?>
			<?php echo $form->error($model,'titulo'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'descripcion'); ?>
			<?php echo $form->textArea($model,'descripcion',array('readonly'=>true,'value'=>$descripcion)); ?>
			<?php echo $form->error($model,'descripcion'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'empleado'); ?>
			<?php echo $form->textField($model,'empleado',array('readonly'=>true,'value'=>$id_usuario)); ?>
			<?php echo $form->error($model,'empleado'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'comentarios'); ?>
			<?php echo $form->textArea($model,'comentarios'); ?>
			<?php echo $form->error($model,'comentarios'); ?>
		</div>

		<div class="row buttons">
			<?php echo CHtml::submitButton('Tomar acción',array('name'=>'botonAceptar')); ?>
			<?php echo CHtml::submitButton('Cancelar',array('name'=>'botonCancelar')); ?>
		</div>

		<?php $this->endWidget(); ?>
		</div><!-- form -->
	<?php } 
?>