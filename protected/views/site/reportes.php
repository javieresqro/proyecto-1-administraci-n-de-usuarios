<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Reportar incidencias';
$this->breadcrumbs=array(
	'Reportar incidencias',
);
?>

<h1>Reporte de incidencias</h1>

<?php if(Yii::app()->user->hasFlash('reportes')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('reportes'); ?>
</div>

<?php else: ?>

<p>Por favor llene el siguiente formulario con los datos de su problema:</p>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Es importante llenar todos los campos.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'titulo'); ?>
		<?php echo $form->textField($model,'titulo'); ?>
		<?php echo $form->error($model,'titulo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textArea($model,'descripcion'); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'area'); ?>
		<?php echo $form->dropDownList($model,'area',array('Redes'=>'Redes', 'Equipo'=>'Equipo', 'Software'=>'Software')); ?>
		<?php echo $form->error($model,'area'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Reportar',array('name'=>'botonReportes')); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
<?php endif; ?>