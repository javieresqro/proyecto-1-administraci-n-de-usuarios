<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'Acerca de', 'url'=>array('/site/page', 'view'=>'about')),
				array('label'=>'Contacto', 'url'=>array('/site/contact')),
				array('label'=>'Login', 'url'=>array('/cruge/ui/login'), 'visible'=>Yii::app()->user->isGuest),
				/*array('label'=>'Administrar usuarios', 'url'=>array('/usuario')),*/
				array('label'=>'Administrar usuarios', 'url'=>Yii::app()->user->ui->userManagementAdminUrl, 'visible'=>Yii::app()->user->getName()=='admin'),
				array('label'=>'Reportar incidencias', 'url'=>array('/site/reportes'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('action_site_reportes') && Yii::app()->user->getName()!='admin'),
				array('label'=>'Ver incidencias no resueltas', 'url'=>array('/site/pendientes'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('action_site_pendientes') && Yii::app()->user->getName()!='admin'),
				array('label'=>'Ver incidencias anteriores', 'url'=>array('/site/anteriores'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('action_site_anteriores') && Yii::app()->user->getName()!='admin'),
				array('label'=>'Incidencias nuevas', 'url'=>$this->createUrl('site/verreportes',array('id'=>'0')), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('action_site_verreportes')),
				array('label'=>'Ver todas las incidencias', 'url'=>array('/site/incidencias'), 'visible'=>!Yii::app()->user->isGuest && Yii::app()->user->checkAccess('action_site_incidencias')),
				array('label'=>'Salir ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div><!-- mainmenu -->


	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->
 <!--<?php echo Yii::app()->user->ui->displayErrorConsole(); ?>-->
</body>
</html>
