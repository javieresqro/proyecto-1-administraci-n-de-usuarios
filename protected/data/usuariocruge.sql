-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 23, 2014 at 06:54 PM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `usuariocruge`
--

-- --------------------------------------------------------

--
-- Table structure for table `cruge_authassignment`
--

CREATE TABLE IF NOT EXISTS `cruge_authassignment` (
  `userid` int(11) NOT NULL,
  `bizrule` text COLLATE utf8_unicode_ci,
  `data` text COLLATE utf8_unicode_ci,
  `itemname` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`userid`,`itemname`),
  KEY `fk_cruge_authassignment_cruge_authitem1` (`itemname`),
  KEY `fk_cruge_authassignment_user` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cruge_authassignment`
--

INSERT INTO `cruge_authassignment` (`userid`, `bizrule`, `data`, `itemname`) VALUES
(2, NULL, 'N;', 'Invitados'),
(4, NULL, 'N;', 'Usuario'),
(6, NULL, 'N;', 'Soporte');

-- --------------------------------------------------------

--
-- Table structure for table `cruge_authitem`
--

CREATE TABLE IF NOT EXISTS `cruge_authitem` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `bizrule` text COLLATE utf8_unicode_ci,
  `data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cruge_authitem`
--

INSERT INTO `cruge_authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('action_site_anteriores', 0, '', NULL, 'N;'),
('action_site_contact', 0, '', NULL, 'N;'),
('action_site_error', 0, '', NULL, 'N;'),
('action_site_incidencias', 0, '', '', 'N;'),
('action_site_index', 0, '', NULL, 'N;'),
('action_site_login', 0, '', NULL, 'N;'),
('action_site_logout', 0, '', NULL, 'N;'),
('action_site_pendientes', 0, '', NULL, 'N;'),
('action_site_reportes', 0, '', NULL, 'N;'),
('action_site_verreportes', 0, '', NULL, 'N;'),
('action_ui_ajaxrbacitemdescr', 0, '', NULL, 'N;'),
('action_ui_editprofile', 0, '', NULL, 'N;'),
('action_ui_fieldsadmincreate', 0, '', NULL, 'N;'),
('action_ui_fieldsadminlist', 0, '', NULL, 'N;'),
('action_ui_rbacajaxgetassignmentbizrule', 0, '', NULL, 'N;'),
('action_ui_rbacajaxsetchilditem', 0, '', NULL, 'N;'),
('action_ui_rbacauthitemchilditems', 0, '', NULL, 'N;'),
('action_ui_rbacauthitemcreate', 0, '', NULL, 'N;'),
('action_ui_rbacauthitemdelete', 0, '', NULL, 'N;'),
('action_ui_rbacauthitemupdate', 0, '', NULL, 'N;'),
('action_ui_rbaclistops', 0, '', NULL, 'N;'),
('action_ui_rbaclistroles', 0, '', NULL, 'N;'),
('action_ui_rbaclisttasks', 0, '', NULL, 'N;'),
('action_ui_rbacusersassignments', 0, '', NULL, 'N;'),
('action_ui_sessionadmin', 0, '', NULL, 'N;'),
('action_ui_systemupdate', 0, '', NULL, 'N;'),
('action_ui_usermanagementadmin', 0, '', NULL, 'N;'),
('action_ui_usermanagementcreate', 0, '', NULL, 'N;'),
('action_ui_usermanagementdelete', 0, '', NULL, 'N;'),
('action_ui_usermanagementupdate', 0, '', NULL, 'N;'),
('action_usuario_admin', 0, '', NULL, 'N;'),
('action_usuario_create', 0, '', NULL, 'N;'),
('action_usuario_delete', 0, '', NULL, 'N;'),
('action_usuario_index', 0, '', NULL, 'N;'),
('action_usuario_update', 0, '', NULL, 'N;'),
('action_usuario_view', 0, '', NULL, 'N;'),
('admin', 0, '', NULL, 'N;'),
('controller_site', 0, '', NULL, 'N;'),
('controller_usuario', 0, '', NULL, 'N;'),
('Crear', 1, ':0 Crear{menu_reportar}{action_site_reportes}', '', 'N;'),
('edit-advanced-profile-features', 0, '/var/www/html/proyecto1/protected/modules/cruge/views/ui/usermanagementupdate.php linea 114', NULL, 'N;'),
('Invitados', 2, '', '', 'N;'),
('menu_reportar', 1, ':Reportar', '', 'N;'),
('Soporte', 2, 'Pueden dar respuesta a las incidencias.', '', 'N;'),
('Usuario', 2, 'Pueden dar de alta incidencias.', '', 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `cruge_authitemchild`
--

CREATE TABLE IF NOT EXISTS `cruge_authitemchild` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cruge_authitemchild`
--

INSERT INTO `cruge_authitemchild` (`parent`, `child`) VALUES
('Usuario', 'action_site_anteriores'),
('Invitados', 'action_site_contact'),
('Soporte', 'action_site_contact'),
('Usuario', 'action_site_contact'),
('Invitados', 'action_site_error'),
('Soporte', 'action_site_error'),
('Usuario', 'action_site_error'),
('Soporte', 'action_site_incidencias'),
('Invitados', 'action_site_index'),
('Soporte', 'action_site_index'),
('Usuario', 'action_site_index'),
('Invitados', 'action_site_login'),
('Soporte', 'action_site_login'),
('Usuario', 'action_site_login'),
('Invitados', 'action_site_logout'),
('Soporte', 'action_site_logout'),
('Usuario', 'action_site_logout'),
('Usuario', 'action_site_pendientes'),
('Crear', 'action_site_reportes'),
('Usuario', 'action_site_reportes'),
('Soporte', 'action_site_verreportes'),
('Invitados', 'controller_site'),
('Soporte', 'controller_site'),
('Usuario', 'controller_site'),
('Soporte', 'controller_usuario'),
('Usuario', 'controller_usuario'),
('menu_reportar', 'Crear'),
('Usuario', 'Crear'),
('Usuario', 'menu_reportar');

-- --------------------------------------------------------

--
-- Table structure for table `cruge_field`
--

CREATE TABLE IF NOT EXISTS `cruge_field` (
  `idfield` int(11) NOT NULL AUTO_INCREMENT,
  `fieldname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `longname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `required` int(11) DEFAULT '0',
  `fieldtype` int(11) DEFAULT '0',
  `fieldsize` int(11) DEFAULT '20',
  `maxlength` int(11) DEFAULT '45',
  `showinreports` int(11) DEFAULT '0',
  `useregexp` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `useregexpmsg` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `predetvalue` mediumblob,
  PRIMARY KEY (`idfield`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cruge_fieldvalue`
--

CREATE TABLE IF NOT EXISTS `cruge_fieldvalue` (
  `idfieldvalue` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `idfield` int(11) NOT NULL,
  `value` blob,
  PRIMARY KEY (`idfieldvalue`),
  KEY `fk_cruge_fieldvalue_cruge_user1` (`iduser`),
  KEY `fk_cruge_fieldvalue_cruge_field1` (`idfield`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cruge_session`
--

CREATE TABLE IF NOT EXISTS `cruge_session` (
  `idsession` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) NOT NULL,
  `created` bigint(30) DEFAULT NULL,
  `expire` bigint(30) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `ipaddress` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usagecount` int(11) DEFAULT '0',
  `lastusage` bigint(30) DEFAULT NULL,
  `logoutdate` bigint(30) DEFAULT NULL,
  `ipaddressout` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idsession`),
  KEY `crugesession_iduser` (`iduser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=142 ;

--
-- Dumping data for table `cruge_session`
--

INSERT INTO `cruge_session` (`idsession`, `iduser`, `created`, `expire`, `status`, `ipaddress`, `usagecount`, `lastusage`, `logoutdate`, `ipaddressout`) VALUES
(1, 1, 1414021163, 1414022963, 1, '127.0.0.1', 1, 1414021163, NULL, NULL),
(2, 1, 1414073314, 1414075114, 0, '127.0.0.1', 1, 1414073314, 1414073376, '127.0.0.1'),
(3, 1, 1414073406, 1414075206, 0, '127.0.0.1', 1, 1414073406, 1414073424, '127.0.0.1'),
(4, 3, 1414073434, 1414075234, 0, '127.0.0.1', 1, 1414073434, 1414073449, '127.0.0.1'),
(5, 1, 1414073456, 1414075256, 1, '127.0.0.1', 1, 1414073456, NULL, NULL),
(6, 3, 1414074525, 1414076325, 0, '127.0.0.1', 1, 1414074525, 1414074538, '127.0.0.1'),
(7, 1, 1414075960, 1414077760, 0, '127.0.0.1', 1, 1414075960, NULL, NULL),
(8, 1, 1414078240, 1414080040, 0, '127.0.0.1', 1, 1414078240, NULL, NULL),
(9, 1, 1414081687, 1414083487, 0, '127.0.0.1', 1, 1414081687, NULL, NULL),
(10, 1, 1414083821, 1414085621, 0, '127.0.0.1', 1, 1414083821, NULL, NULL),
(11, 1, 1414085870, 1414087670, 0, '127.0.0.1', 1, 1414085870, NULL, NULL),
(12, 1, 1414088558, 1414090358, 0, '127.0.0.1', 1, 1414088558, NULL, NULL),
(13, 1, 1414090757, 1414092557, 1, '127.0.0.1', 1, 1414090757, NULL, NULL),
(14, 1, 1414094519, 1414096319, 0, '127.0.0.1', 1, 1414094519, NULL, NULL),
(15, 1, 1414096410, 1414098210, 0, '127.0.0.1', 1, 1414096410, 1414096745, '127.0.0.1'),
(16, 1, 1414096753, 1414098553, 0, '127.0.0.1', 1, 1414096753, 1414096907, '127.0.0.1'),
(17, 5, 1414096914, 1414098714, 0, '127.0.0.1', 1, 1414096914, 1414096922, '127.0.0.1'),
(18, 1, 1414096927, 1414098727, 0, '127.0.0.1', 1, 1414096927, 1414097174, '127.0.0.1'),
(19, 5, 1414097188, 1414098988, 0, '127.0.0.1', 1, 1414097188, 1414097196, '127.0.0.1'),
(20, 1, 1414097213, 1414099013, 0, '127.0.0.1', 1, 1414097213, 1414097227, '127.0.0.1'),
(21, 5, 1414097232, 1414099032, 0, '127.0.0.1', 1, 1414097232, 1414097234, '127.0.0.1'),
(22, 1, 1414097239, 1414099039, 0, '127.0.0.1', 1, 1414097239, 1414097327, '127.0.0.1'),
(23, 1, 1414097364, 1414099164, 0, '127.0.0.1', 1, 1414097364, 1414097412, '127.0.0.1'),
(24, 4, 1414097418, 1414099218, 0, '127.0.0.1', 1, 1414097418, 1414097448, '127.0.0.1'),
(25, 1, 1414097452, 1414099252, 0, '127.0.0.1', 1, 1414097452, 1414097884, '127.0.0.1'),
(26, 1, 1414098149, 1414099949, 0, '127.0.0.1', 1, 1414098149, 1414098153, '127.0.0.1'),
(27, 5, 1414098158, 1414099958, 0, '127.0.0.1', 1, 1414098158, 1414098161, '127.0.0.1'),
(28, 1, 1414098213, 1414100013, 0, '127.0.0.1', 1, 1414098213, 1414098411, '127.0.0.1'),
(29, 1, 1414098639, 1414100439, 0, '127.0.0.1', 1, 1414098639, 1414098665, '127.0.0.1'),
(30, 1, 1414098826, 1414100626, 0, '127.0.0.1', 1, 1414098826, 1414098940, '127.0.0.1'),
(31, 4, 1414098945, 1414100745, 0, '127.0.0.1', 1, 1414098945, 1414098953, '127.0.0.1'),
(32, 5, 1414098964, 1414100764, 0, '127.0.0.1', 1, 1414098964, 1414098967, '127.0.0.1'),
(33, 1, 1414099011, 1414100811, 0, '127.0.0.1', 1, 1414099011, 1414099073, '127.0.0.1'),
(34, 1, 1414099077, 1414100877, 0, '127.0.0.1', 1, 1414099077, 1414099082, '127.0.0.1'),
(35, 4, 1414099088, 1414100888, 0, '127.0.0.1', 1, 1414099088, 1414099092, '127.0.0.1'),
(36, 1, 1414099096, 1414100896, 0, '127.0.0.1', 1, 1414099096, 1414099121, '127.0.0.1'),
(37, 4, 1414099127, 1414100927, 0, '127.0.0.1', 1, 1414099127, 1414099136, '127.0.0.1'),
(38, 1, 1414099145, 1414100945, 0, '127.0.0.1', 1, 1414099145, 1414099174, '127.0.0.1'),
(39, 4, 1414099179, 1414100979, 0, '127.0.0.1', 1, 1414099179, 1414099183, '127.0.0.1'),
(40, 4, 1414099190, 1414100990, 0, '127.0.0.1', 1, 1414099190, 1414099193, '127.0.0.1'),
(41, 1, 1414099197, 1414100997, 0, '127.0.0.1', 1, 1414099197, 1414099268, '127.0.0.1'),
(42, 4, 1414099279, 1414101079, 0, '127.0.0.1', 1, 1414099279, 1414099309, '127.0.0.1'),
(43, 1, 1414099314, 1414101114, 0, '127.0.0.1', 1, 1414099314, 1414099387, '127.0.0.1'),
(44, 4, 1414099396, 1414101196, 0, '127.0.0.1', 1, 1414099396, 1414099407, '127.0.0.1'),
(45, 4, 1414099438, 1414101238, 0, '127.0.0.1', 1, 1414099438, 1414099456, '127.0.0.1'),
(46, 1, 1414099462, 1414101262, 0, '127.0.0.1', 1, 1414099462, 1414099518, '127.0.0.1'),
(47, 4, 1414099530, 1414101330, 0, '127.0.0.1', 1, 1414099530, 1414099535, '127.0.0.1'),
(48, 1, 1414099567, 1414101367, 0, '127.0.0.1', 1, 1414099567, 1414099657, '127.0.0.1'),
(49, 4, 1414099662, 1414101462, 0, '127.0.0.1', 1, 1414099662, 1414099677, '127.0.0.1'),
(50, 1, 1414099681, 1414101481, 0, '127.0.0.1', 1, 1414099681, 1414099821, '127.0.0.1'),
(51, 4, 1414099870, 1414101670, 0, '127.0.0.1', 1, 1414099870, 1414099875, '127.0.0.1'),
(52, 1, 1414099880, 1414101680, 0, '127.0.0.1', 1, 1414099880, 1414100355, '127.0.0.1'),
(53, 4, 1414100360, 1414102160, 0, '127.0.0.1', 1, 1414100360, 1414100537, '127.0.0.1'),
(54, 1, 1414100541, 1414102341, 0, '127.0.0.1', 1, 1414100541, 1414100944, '127.0.0.1'),
(55, 4, 1414100951, 1414102751, 0, '127.0.0.1', 1, 1414100951, 1414101342, '127.0.0.1'),
(56, 1, 1414101346, 1414103146, 0, '127.0.0.1', 1, 1414101346, 1414101378, '127.0.0.1'),
(57, 4, 1414101384, 1414103184, 0, '127.0.0.1', 1, 1414101384, 1414101402, '127.0.0.1'),
(58, 1, 1414101408, 1414103208, 0, '127.0.0.1', 1, 1414101408, 1414101591, '127.0.0.1'),
(59, 1, 1414101597, 1414103397, 0, '127.0.0.1', 1, 1414101597, 1414101716, '127.0.0.1'),
(60, 4, 1414101722, 1414103522, 0, '127.0.0.1', 1, 1414101722, 1414101728, '127.0.0.1'),
(61, 1, 1414101732, 1414103532, 0, '127.0.0.1', 1, 1414101732, 1414101752, '127.0.0.1'),
(62, 1, 1414101757, 1414103557, 0, '127.0.0.1', 1, 1414101757, 1414101896, '127.0.0.1'),
(63, 4, 1414101901, 1414103701, 0, '127.0.0.1', 1, 1414101901, 1414102002, '127.0.0.1'),
(64, 1, 1414102006, 1414103806, 0, '127.0.0.1', 1, 1414102006, 1414102030, '127.0.0.1'),
(65, 4, 1414102036, 1414103836, 0, '127.0.0.1', 1, 1414102036, 1414102133, '127.0.0.1'),
(66, 1, 1414102170, 1414103970, 0, '127.0.0.1', 1, 1414102170, 1414102295, '127.0.0.1'),
(67, 4, 1414102300, 1414104100, 0, '127.0.0.1', 1, 1414102300, 1414102451, '127.0.0.1'),
(68, 4, 1414102457, 1414104257, 0, '127.0.0.1', 1, 1414102457, 1414102461, '127.0.0.1'),
(69, 5, 1414102468, 1414104268, 0, '127.0.0.1', 1, 1414102468, 1414102470, '127.0.0.1'),
(70, 1, 1414102497, 1414104297, 0, '127.0.0.1', 1, 1414102497, 1414102560, '127.0.0.1'),
(71, 1, 1414102565, 1414104365, 0, '127.0.0.1', 1, 1414102565, 1414102567, '127.0.0.1'),
(72, 5, 1414102572, 1414104372, 0, '127.0.0.1', 1, 1414102572, 1414102577, '127.0.0.1'),
(73, 5, 1414102619, 1414104419, 0, '127.0.0.1', 1, 1414102619, 1414102638, '127.0.0.1'),
(74, 1, 1414102649, 1414104449, 0, '127.0.0.1', 1, 1414102649, 1414102717, '127.0.0.1'),
(75, 5, 1414102723, 1414104523, 0, '127.0.0.1', 1, 1414102723, 1414102725, '127.0.0.1'),
(76, 1, 1414102791, 1414104591, 0, '127.0.0.1', 1, 1414102791, 1414102816, '127.0.0.1'),
(77, 1, 1414102825, 1414104625, 0, '127.0.0.1', 1, 1414102825, 1414102862, '127.0.0.1'),
(78, 1, 1414103030, 1414104830, 0, '127.0.0.1', 1, 1414103030, 1414103033, '127.0.0.1'),
(79, 4, 1414103040, 1414104840, 0, '127.0.0.1', 1, 1414103040, 1414103068, '127.0.0.1'),
(80, 1, 1414103072, 1414104872, 0, '127.0.0.1', 1, 1414103072, 1414103487, '127.0.0.1'),
(81, 1, 1414103497, 1414105297, 0, '127.0.0.1', 1, 1414103497, 1414103555, '127.0.0.1'),
(82, 1, 1414103560, 1414105360, 0, '127.0.0.1', 1, 1414103560, 1414103650, '127.0.0.1'),
(83, 1, 1414103655, 1414105455, 0, '127.0.0.1', 1, 1414103655, 1414103709, '127.0.0.1'),
(84, 5, 1414103715, 1414105515, 0, '127.0.0.1', 1, 1414103715, 1414103723, '127.0.0.1'),
(85, 5, 1414103728, 1414105528, 0, '127.0.0.1', 1, 1414103728, 1414103729, '127.0.0.1'),
(86, 4, 1414103734, 1414105534, 0, '127.0.0.1', 1, 1414103734, 1414103735, '127.0.0.1'),
(87, 5, 1414103785, 1414105585, 0, '127.0.0.1', 1, 1414103785, 1414103786, '127.0.0.1'),
(88, 4, 1414103796, 1414105596, 0, '127.0.0.1', 1, 1414103796, 1414103798, '127.0.0.1'),
(89, 1, 1414103806, 1414105606, 0, '127.0.0.1', 1, 1414103806, 1414103884, '127.0.0.1'),
(90, 4, 1414103930, 1414105730, 0, '127.0.0.1', 1, 1414103930, 1414103932, '127.0.0.1'),
(91, 5, 1414103940, 1414105740, 0, '127.0.0.1', 1, 1414103940, 1414103944, '127.0.0.1'),
(92, 1, 1414103952, 1414105752, 0, '127.0.0.1', 1, 1414103952, 1414104000, '127.0.0.1'),
(93, 4, 1414104007, 1414105807, 0, '127.0.0.1', 1, 1414104007, 1414104009, '127.0.0.1'),
(94, 5, 1414104016, 1414105816, 0, '127.0.0.1', 1, 1414104016, 1414104025, '127.0.0.1'),
(95, 1, 1414104029, 1414105829, 0, '127.0.0.1', 1, 1414104029, 1414104054, '127.0.0.1'),
(96, 4, 1414104059, 1414105859, 0, '127.0.0.1', 1, 1414104059, 1414104061, '127.0.0.1'),
(97, 5, 1414104066, 1414105866, 0, '127.0.0.1', 1, 1414104066, 1414104076, '127.0.0.1'),
(98, 1, 1414104080, 1414105880, 0, '127.0.0.1', 1, 1414104080, 1414104136, '127.0.0.1'),
(99, 5, 1414104141, 1414105941, 0, '127.0.0.1', 1, 1414104141, 1414104142, '127.0.0.1'),
(100, 1, 1414104147, 1414105947, 0, '127.0.0.1', 1, 1414104147, 1414104188, '127.0.0.1'),
(101, 5, 1414104193, 1414105993, 0, '127.0.0.1', 1, 1414104193, 1414104199, '127.0.0.1'),
(102, 5, 1414104209, 1414106009, 0, '127.0.0.1', 1, 1414104209, 1414104268, '127.0.0.1'),
(103, 4, 1414104273, 1414106073, 0, '127.0.0.1', 1, 1414104273, 1414104276, '127.0.0.1'),
(104, 1, 1414104322, 1414106122, 0, '127.0.0.1', 1, 1414104322, 1414104354, '127.0.0.1'),
(105, 1, 1414104391, 1414106191, 0, '127.0.0.1', 1, 1414104391, 1414104393, '127.0.0.1'),
(106, 4, 1414104398, 1414106198, 0, '127.0.0.1', 1, 1414104398, 1414104403, '127.0.0.1'),
(107, 5, 1414104409, 1414106209, 0, '127.0.0.1', 1, 1414104409, 1414104416, '127.0.0.1'),
(108, 1, 1414104420, 1414106220, 0, '127.0.0.1', 1, 1414104420, 1414104480, '127.0.0.1'),
(109, 5, 1414104487, 1414106287, 0, '127.0.0.1', 1, 1414104487, 1414104489, '127.0.0.1'),
(110, 1, 1414104494, 1414106294, 0, '127.0.0.1', 1, 1414104494, 1414104526, '127.0.0.1'),
(111, 1, 1414104553, 1414106353, 0, '127.0.0.1', 1, 1414104553, 1414104588, '127.0.0.1'),
(112, 5, 1414104594, 1414106394, 0, '127.0.0.1', 1, 1414104594, 1414104598, '127.0.0.1'),
(113, 1, 1414104608, 1414106408, 0, '127.0.0.1', 1, 1414104608, 1414104609, '127.0.0.1'),
(114, 1, 1414104648, 1414106448, 0, '127.0.0.1', 1, 1414104648, 1414104650, '127.0.0.1'),
(115, 5, 1414104656, 1414106456, 0, '127.0.0.1', 1, 1414104656, 1414104671, '127.0.0.1'),
(116, 1, 1414104676, 1414106476, 0, '127.0.0.1', 1, 1414104676, 1414104721, '127.0.0.1'),
(117, 6, 1414104726, 1414106526, 0, '127.0.0.1', 1, 1414104726, 1414104828, '127.0.0.1'),
(118, 6, 1414104834, 1414106634, 0, '127.0.0.1', 1, 1414104834, 1414104841, '127.0.0.1'),
(119, 1, 1414104845, 1414106645, 0, '127.0.0.1', 1, 1414104845, 1414104983, '127.0.0.1'),
(120, 6, 1414104989, 1414106789, 0, '127.0.0.1', 1, 1414104989, 1414104992, '127.0.0.1'),
(121, 6, 1414105153, 1414106953, 0, '127.0.0.1', 1, 1414105153, 1414105220, '127.0.0.1'),
(122, 1, 1414105223, 1414107023, 0, '127.0.0.1', 1, 1414105223, 1414105239, '127.0.0.1'),
(123, 6, 1414105244, 1414107044, 0, '127.0.0.1', 1, 1414105244, 1414105248, '127.0.0.1'),
(124, 4, 1414105255, 1414107055, 0, '127.0.0.1', 1, 1414105255, 1414105258, '127.0.0.1'),
(125, 4, 1414105426, 1414107226, 0, '127.0.0.1', 1, 1414105426, 1414105472, '127.0.0.1'),
(126, 6, 1414105478, 1414107278, 0, '127.0.0.1', 1, 1414105478, 1414105532, '127.0.0.1'),
(127, 1, 1414105536, 1414107336, 0, '127.0.0.1', 1, 1414105536, 1414106326, '127.0.0.1'),
(128, 1, 1414106353, 1414108153, 0, '127.0.0.1', 1, 1414106353, 1414106376, '127.0.0.1'),
(129, 4, 1414106381, 1414108181, 0, '127.0.0.1', 1, 1414106381, 1414106689, '127.0.0.1'),
(130, 1, 1414106694, 1414108494, 0, '127.0.0.1', 1, 1414106694, 1414106705, '127.0.0.1'),
(131, 4, 1414106715, 1414108515, 0, '127.0.0.1', 1, 1414106715, 1414107169, '127.0.0.1'),
(132, 1, 1414107173, 1414108973, 0, '127.0.0.1', 1, 1414107173, 1414107186, '127.0.0.1'),
(133, 6, 1414107191, 1414108991, 0, '127.0.0.1', 1, 1414107191, 1414107193, '127.0.0.1'),
(134, 4, 1414107200, 1414109000, 0, '127.0.0.1', 1, 1414107200, 1414107255, '127.0.0.1'),
(135, 4, 1414107301, 1414109101, 0, '127.0.0.1', 1, 1414107301, 1414107362, '127.0.0.1'),
(136, 6, 1414107368, 1414109168, 0, '127.0.0.1', 1, 1414107368, 1414108213, '127.0.0.1'),
(137, 4, 1414108239, 1414110039, 0, '127.0.0.1', 1, 1414108239, 1414108261, '127.0.0.1'),
(138, 6, 1414108270, 1414110070, 0, '127.0.0.1', 1, 1414108270, 1414108332, '127.0.0.1'),
(139, 6, 1414108337, 1414110137, 0, '127.0.0.1', 1, 1414108337, 1414108393, '127.0.0.1'),
(140, 4, 1414108398, 1414110198, 0, '127.0.0.1', 1, 1414108398, 1414108402, '127.0.0.1'),
(141, 4, 1414108416, 1414110216, 0, '127.0.0.1', 1, 1414108416, 1414108421, '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `cruge_system`
--

CREATE TABLE IF NOT EXISTS `cruge_system` (
  `idsystem` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `largename` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sessionmaxdurationmins` int(11) DEFAULT '30',
  `sessionmaxsameipconnections` int(11) DEFAULT '10',
  `sessionreusesessions` int(11) DEFAULT '1' COMMENT '1yes 0no',
  `sessionmaxsessionsperday` int(11) DEFAULT '-1',
  `sessionmaxsessionsperuser` int(11) DEFAULT '-1',
  `systemnonewsessions` int(11) DEFAULT '0' COMMENT '1yes 0no',
  `systemdown` int(11) DEFAULT '0',
  `registerusingcaptcha` int(11) DEFAULT '0',
  `registerusingterms` int(11) DEFAULT '0',
  `terms` blob,
  `registerusingactivation` int(11) DEFAULT '1',
  `defaultroleforregistration` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registerusingtermslabel` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registrationonlogin` int(11) DEFAULT '1',
  PRIMARY KEY (`idsystem`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `cruge_system`
--

INSERT INTO `cruge_system` (`idsystem`, `name`, `largename`, `sessionmaxdurationmins`, `sessionmaxsameipconnections`, `sessionreusesessions`, `sessionmaxsessionsperday`, `sessionmaxsessionsperuser`, `systemnonewsessions`, `systemdown`, `registerusingcaptcha`, `registerusingterms`, `terms`, `registerusingactivation`, `defaultroleforregistration`, `registerusingtermslabel`, `registrationonlogin`) VALUES
(1, 'default', NULL, 30, 10, 1, -1, -1, 0, 0, 0, 0, '', 0, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cruge_user`
--

CREATE TABLE IF NOT EXISTS `cruge_user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `regdate` bigint(30) DEFAULT NULL,
  `actdate` bigint(30) DEFAULT NULL,
  `logondate` bigint(30) DEFAULT NULL,
  `username` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Hashed password',
  `authkey` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'llave de autentificacion',
  `state` int(11) DEFAULT '0',
  `totalsessioncounter` int(11) DEFAULT '0',
  `currentsessioncounter` int(11) DEFAULT '0',
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `cruge_user`
--

INSERT INTO `cruge_user` (`iduser`, `regdate`, `actdate`, `logondate`, `username`, `email`, `password`, `authkey`, `state`, `totalsessioncounter`, `currentsessioncounter`) VALUES
(1, NULL, NULL, 1414107173, 'admin', 'admin@tucorreo.com', 'admin', NULL, 1, 0, 0),
(2, NULL, NULL, NULL, 'invitado', 'invitado', 'nopassword', NULL, 1, 0, 0),
(4, 1414096860, NULL, 1414108416, 'usuario1', 'usuario1@hotmail.com', 'usuario1', '776d77db59377ca6d401d69f2eea845b', 1, 0, 0),
(6, 1414104701, NULL, 1414108337, 'soporte1', 'soporte1@a.com', 'soporte1', '13830aab732b0560ba426a0d83b0ba7c', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `empleados`
--

CREATE TABLE IF NOT EXISTS `empleados` (
  `id_usuario` int(11) unsigned NOT NULL,
  `especialidad` set('Redes','Equipos','Software') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `especialidad` (`especialidad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `empleados`
--

INSERT INTO `empleados` (`id_usuario`, `especialidad`) VALUES
(6, 'Redes');

-- --------------------------------------------------------

--
-- Table structure for table `incidencias`
--

CREATE TABLE IF NOT EXISTS `incidencias` (
  `id_incidencia` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) unsigned NOT NULL,
  `id_empleado` int(11) unsigned NOT NULL,
  `titulo` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `estatus` set('Nueva','Pendiente','Resuelta') COLLATE utf8_unicode_ci NOT NULL,
  `area` set('Redes','Equipos','Software') COLLATE utf8_unicode_ci NOT NULL,
  `comentarios` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_incidencia`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `incidencias`
--

INSERT INTO `incidencias` (`id_incidencia`, `id_usuario`, `id_empleado`, `titulo`, `descripcion`, `fecha`, `hora`, `estatus`, `area`, `comentarios`) VALUES
(2, 4, 4, 'Inc 1', 'Se trabÃ³ la mÃ¡quina.', '2014-10-23', '18:50:54', 'Pendiente', 'Redes', 'Me parece terrible.');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cruge_authassignment`
--
ALTER TABLE `cruge_authassignment`
  ADD CONSTRAINT `fk_cruge_authassignment_cruge_authitem1` FOREIGN KEY (`itemname`) REFERENCES `cruge_authitem` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cruge_authassignment_user` FOREIGN KEY (`userid`) REFERENCES `cruge_user` (`iduser`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `cruge_authitemchild`
--
ALTER TABLE `cruge_authitemchild`
  ADD CONSTRAINT `crugeauthitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `cruge_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `crugeauthitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `cruge_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cruge_fieldvalue`
--
ALTER TABLE `cruge_fieldvalue`
  ADD CONSTRAINT `fk_cruge_fieldvalue_cruge_field1` FOREIGN KEY (`idfield`) REFERENCES `cruge_field` (`idfield`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cruge_fieldvalue_cruge_user1` FOREIGN KEY (`iduser`) REFERENCES `cruge_user` (`iduser`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
